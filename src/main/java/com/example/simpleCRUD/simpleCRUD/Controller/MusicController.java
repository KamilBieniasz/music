package com.example.simpleCRUD.simpleCRUD.Controller;

import com.example.simpleCRUD.simpleCRUD.Service.MusicService;
import com.example.simpleCRUD.simpleCRUD.dto.Music;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("music")
public class MusicController {
    private final MusicService musicService;

    @Autowired
    public MusicController(MusicService musicService) {

        this.musicService = musicService;
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello(){
        return new ResponseEntity<>("witam", HttpStatus.OK);
    }

    @GetMapping("/get")
    public ResponseEntity<ArrayList<Music>> getMusic(){
        return new ResponseEntity<>(musicService.returnMusic(), HttpStatus.OK);
    }
    @PostMapping("/add")
    public ResponseEntity<Music> addMusic(@RequestBody Music music){
        musicService.addMusic(music);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
