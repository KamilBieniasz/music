package com.example.simpleCRUD.simpleCRUD.Service;

import com.example.simpleCRUD.simpleCRUD.Repository.MusicRepository;
import com.example.simpleCRUD.simpleCRUD.dto.Music;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Array;
import java.util.ArrayList;

@Service
public class MusicServiceImpl implements MusicService{
    private final MusicRepository musicRepository;

    public MusicServiceImpl(MusicRepository musicRepository) {
        this.musicRepository = musicRepository;
    }

    @Override
    public ArrayList<Music> returnMusic() {
        return musicRepository.getMusic();
    }

    @Override
    public void addMusic(Music music) {
        musicRepository.addMusic(music);
    }
}
