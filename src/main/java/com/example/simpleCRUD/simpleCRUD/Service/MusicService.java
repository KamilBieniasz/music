package com.example.simpleCRUD.simpleCRUD.Service;

import com.example.simpleCRUD.simpleCRUD.dto.Music;

import java.util.ArrayList;

public interface MusicService {

    ArrayList<Music> returnMusic();
    void addMusic(Music music);
}
