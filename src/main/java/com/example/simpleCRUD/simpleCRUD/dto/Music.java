package com.example.simpleCRUD.simpleCRUD.dto;

public class Music {
    private String title;
    private String artists;
    private int year;

    public Music() {
    }

    public Music(String title, String artists, int year) {
        this.title = title;
        this.artists = artists;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public String getArtists() {
        return artists;
    }

    public int getYear() {
        return year;
    }
}
