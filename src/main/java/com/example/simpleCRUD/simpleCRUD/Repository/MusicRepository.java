package com.example.simpleCRUD.simpleCRUD.Repository;

import com.example.simpleCRUD.simpleCRUD.dto.Music;

import java.util.ArrayList;

public interface MusicRepository {
    ArrayList<Music> getMusic();
    void addMusic(Music music);
}
