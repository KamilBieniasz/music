package com.example.simpleCRUD.simpleCRUD.Repository;

import com.example.simpleCRUD.simpleCRUD.dto.Music;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public class MusicRepositoryImpl implements MusicRepository {
    ArrayList<Music> musics = new ArrayList<>();

    public MusicRepositoryImpl() {
        musics.add(new Music("Ostatni krzyk osiedla", "Paluch", 2008));
        musics.add(new Music("Egzotyka", "Quebonafide", 2012));
        musics.add(new Music("Nadciśnienie", "Paluch", 2020));
    }

    @Override
    public ArrayList<Music> getMusic() {
        return musics;
    }

    @Override
    public void addMusic(Music music) {
        musics.add(new Music(music.getTitle(), music.getArtists(), music.getYear()));
    }
}
